# Migration Notice

[Kuno was migrated to Codeberg,](https://codeberg.org/ssdlk/kuno#rebuild-notice) so this page is archived & abandoned; <br>
it will eventually be properly deteled — for GitLab's sake.

<hr>
<br>

## Kuno

A mod that optimizes [**Dying Light (_DL_)**](https://en.wikipedia.org/wiki/Dying_Light) without compromising graphics (_optional_) or multiplayer.

Kuno can fix: low FPS, stutters/instability in FPS, input lag, and resource hogging — on both Windows and Linux.

<hr>