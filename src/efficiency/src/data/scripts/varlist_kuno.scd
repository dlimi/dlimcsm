!include("varlist_kn_shadows.scr")
!include("varlist_kn_clouds.scr")
!include("varlist_kn_lighting.scr")

!include("varlist_kn_blur.scr")

!include("varlist_kn_nvidia_fx.scr")
!include("varlist_kn_gfx.scr")

!include("varlist_kn_overlays.scr")
!include("varlist_kn_water.scr")