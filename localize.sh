#!/usr/bin/env bash

#This is free and unencumbered software released into the public domain.
#
#Anyone is free to do anything, by any means, with this software.
#
#This software is provided "as is", without any kind of warranty: explicit, or implicit.
#
#For more information, please refer to <http://unlicense.org/>

clear

builds=("efficiency" "performance") #kuno build types
locales=("Br" "Cn" "Cs" "De" "El" "En" "Es" "Fr" "It" "Jp" "Ko" "Nl" "Pl" "Ru" "Th" "Tr" "Tw") #`srctree` localizations

extras=("LICENSE" "VERSION")

pak_file="*.pak"

function localize() {
	printf "Localizing: ${input_file}\n"
	printf "——————————————————————————————\n"

	for locale in "${locales[@]}"; do #Localize the build(s) by iterating through the locales array.
		cp ${input_file} ${output_dir}/Data${locale}.pak

		printf "	—> Data%s.pak\n" "$locale"
	done

	printf "——————————————————————————————\n"
}

which lokalize &> /dev/null #Check if `lokalize` is installed.

if [ "$?" -eq 0 ]; then
	lokalize_installed=true
else
	lokalize_installed=false
fi

for build in "${builds[@]}" #Iterate through each build type.
do
	input_file="pak/${build}/${build}.pak"
	output_dir="pak/${build}/DW_DLC2"

	if [ ! -d "${output_dir}" ]; then #Make sure the directory exists, and that there are no files in the way.
		mkdir ${output_dir}

		for extra in "${extras[@]}"; do
			cp ${extra} ${output_dir}/
		done
	elif [ -f "${output_dir}"/"${pak_file}" ]; then
		rm -rf ${output_dir}/${pak_file}
	fi

	if [ "$lokalize_installed" = true ]; then
		lokalize ${input_file}
		printf "\n\n"

		for locale in "${locales[@]}"; do
			mv Data${locale}.pak ${output_dir}/ #Manually move the files, as `lokalize` does not currently support output directories.
		done

	else
		localize #If `lokalize` is not installed then use the fallback implementation in Bash.
	fi

done
